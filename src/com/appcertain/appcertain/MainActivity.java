package com.appcertain.appcertain;

import java.io.BufferedReader;  // used to convert stream to string
import java.io.IOException;  // used to handle I/O exceptions
import java.io.InputStream; // used with Buffered Reader
import java.io.InputStreamReader; // see above
import java.net.HttpURLConnection; // used to connect to the web
import java.net.MalformedURLException; // handles url probs
import java.net.URL; // Allows storing of URL variables

import org.json.JSONArray; // JSON Array functions
import org.json.JSONException; // used to handle any JSON problems
import org.json.JSONObject; // JSON Object functions

import android.os.Bundle; // Android bundle, aka state saver
import android.app.Activity; // used to run Android activites
import android.view.Menu; // handles menus
import android.webkit.WebView; // webview functions

public class MainActivity extends Activity { // class for our main activity
	
	String defaultimage = "http://www.appcertain.com/blog/wp-content/uploads/2013/01/app_store.png";  // fallback image
	
	@Override
	protected void onCreate(Bundle savedInstanceState) { // runs when the application opens
		
		String appcjsonpage; // creates the string where the JSON data will go.
		Integer count = 0; // creates a counter and sets it to 0.
		try { // try used because JSON problems may arise
			appcjsonpage = getListings(); //defers to getListings to get data
			JSONObject jsonObj = new JSONObject(appcjsonpage); //creates a JSON object from the data
			JSONArray content = jsonObj.getJSONArray("apps"); // focuses in on the "apps"
			String array = content.toString(); // turns the "apps" listing to a string
            
			String output = "<!DOCTYPE HTML SYSTEM><html><head><title>AppCertain</title></head><body>";  // creates start of webview
            int contentlength = content.length(); // saves how many apps we have listed
			while (count < contentlength){  // while loop to go through each app
            	JSONObject appinfo = content.getJSONObject(count); // pulls the JSON object for the app
            	String version = appinfo.getString("Version"); // gets the version # of the app
                String name = appinfo.getString("Name"); // Gets the name of the app
                String id = appinfo.getString("Identifier"); // gets the identifier of the app
                String appleinfo = getAppInfo(id); // takes the id and gets the apple info
                JSONObject appleObj = new JSONObject(appleinfo);  // creates the JSON object of the apple info
                int numofresults = appleObj.getInt("resultCount"); // saves how many apps we got from the apple search
                Integer num = count + 1; // ups the counter by 1
            	String countstr = num.toString(); // converts the counter number to a string
            	
                if (numofresults == 0){ // what to do if it isn't in the app store
                	output = output.concat("<p>" + countstr + ". "+ "<img src=\"" + defaultimage + "\" alt=\"apppic\" width=\"15\" height=\"15\">" + " Name: " + name + "</p><p>Identifier: " + id + "</p><p>Version: " + version + "</p><p> </p><p></p>");  // sticks the default image and fills in the rest of the data
                    count = count + 1; // moves the counter up by one
                    continue; // moves on to the next app
                }
    			JSONArray results = appleObj.getJSONArray("results"); // gets the results from the apple JSON request
    			JSONObject theresult = results.getJSONObject(0); // takes the first result (the most likely one)
    			String appimage = theresult.getString("artworkUrl512"); // grabs the image URL
    			
                
                output = output.concat("<p>" + countstr + ". "+ "<img src=\"" + appimage + "\" alt=\"apppic\" width=\"15\" height=\"15\">" + " Name: " + name + "</p><p>Identifier: " + id + "</p><p>Version: " + version + "</p><p> </p><p></p>");  // fills in the app data
                count = count + 1; //ups the counter by one
            }
            output = output + "</body></html>"; // finishes the html code
            WebView webView = new WebView(this)	; // creates a new webview
			
			webView.loadData(output,"text/html",null); // loads the HTML data
			setContentView(webView); // displays the webview
		
		} catch (MalformedURLException e) { // prints the stack trace if the url doesn't work
			e.printStackTrace();
		} catch (IOException e) { // prints the stack trace if there is an IO exception
			e.printStackTrace();
		} catch (JSONException e) { // prints the stack trace if there is a JSON exception
			e.printStackTrace();
		}
		
		super.onCreate(savedInstanceState);
	}
	
	private String getListings() throws MalformedURLException, IOException { //connects and downloads the top ten app JSON file
	    HttpURLConnection con = (HttpURLConnection) new URL("http://www.appcertain.com/top-ten-apps.json").openConnection();
	    con.connect();

	    if (con.getResponseCode() == HttpURLConnection.HTTP_OK) { // if we get a OK from the server, get the data
	        return inputStreamToString(con.getInputStream());
	    } else { // otherwise return nothing
	        return null;
	    }
	}
	private String getAppInfo(String appid) throws MalformedURLException, IOException { // grabs the apple data, see above
	    HttpURLConnection con = (HttpURLConnection) new URL("https://itunes.apple.com/lookup?bundleId=" + appid).openConnection();
	    con.connect();

	    if (con.getResponseCode() == HttpURLConnection.HTTP_OK) {
	        return inputStreamToString(con.getInputStream());
	    } else {
	        return null;
	    }
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) { // opens the app menu
		
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}
	private String inputStreamToString(InputStream in) throws IOException { // converts the stream to a string
	    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(in));
	    StringBuilder stringBuilder = new StringBuilder();
	    String line = null;

	    while ((line = bufferedReader.readLine()) != null) {
	        stringBuilder.append(line + "\n");
	    }

	    bufferedReader.close();
	    return stringBuilder.toString();
	}
	
	}


